#!/bin/sh

vtctlclient -server=localhost:15999 ApplySchema -sql="$(cat dbregistmain.sql)" dbregistmain
vtctlclient -server=localhost:15999 ApplyVSchema -vschema="$(cat vschema_dbregistmain.json)" dbregistmain

vtctlclient -server=localhost:15999 ApplySchema -sql="$(cat dbregistonl.sql)" dbregistonl
vtctlclient -server=localhost:15999 ApplyVSchema -vschema="$(cat vschema_dbregistonl.json)" dbregistonl
