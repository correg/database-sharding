CREATE TABLE `tbl_onl` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) NOT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) NOT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) NOT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) NOT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) NOT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `crelec` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตบรรยาย',
  `crelab` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตปฏิบัติ',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip ผู้ใช้',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `random_no` int(11) DEFAULT NULL COMMENT 'เลขสุ่ม',
  `reason_id` char(2) DEFAULT NULL COMMENT 'เหตุผลจากตารารง tbl_reason',
  `rank_no` int(2) DEFAULT '99' COMMENT 'ลำดับความต้องการ   1 คือมากที่สุด,   99=default'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_lab` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) NOT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) NOT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) NOT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) NOT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) NOT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip ผู้ใช้',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `reason_id` char(2) DEFAULT NULL COMMENT 'เหตุผลจากตารารง tbl_reason'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_onl`
  ADD PRIMARY KEY (`semester`,`year`,`student_id`,`courseno`,`seclec`,`seclab`,`regist_type`) USING BTREE,
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`) USING BTREE,
  ADD KEY `semester` (`semester`),
  ADD KEY `year` (`year`),
  ADD KEY `courseno` (`courseno`);

ALTER TABLE `tbl_onl_lab`
  ADD PRIMARY KEY (`semester`,`year`,`student_id`,`courseno`,`seclec`,`seclab`,`regist_type`) USING BTREE,
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`),
  ADD KEY `semester` (`semester`),
  ADD KEY `year` (`year`),
  ADD KEY `courseno` (`courseno`),
  ADD KEY `seclec` (`seclec`),
  ADD KEY `seclab` (`seclab`);
