CREATE TABLE `tbl_bulletin` (
`bulletin_id` varchar(8)
,`courseno` varchar(6)
,`courseno_en` varchar(15)
,`courseno_th` varchar(30)
,`title_short_en` varchar(100)
,`title_long_en` varchar(400)
,`title_long_th` varchar(400)
,`degree_level` varchar(1)
,`course_type` varchar(1)
,`crelec` decimal(5,2)
,`crelab` decimal(5,2)
,`credit` varchar(30)
,`formerly` varchar(6)
,`formerly_bulletin_id` varchar(8)
,`transfer_to` varchar(6)
,`transfer_to_bulletin_id` varchar(8)
,`grade_id` varchar(1)
,`grade_p_accept` varchar(1)
,`course_desc_en` varchar(2000)
,`course_desc_th` varchar(2000)
,`pre_en` varchar(400)
,`pre_th` varchar(400)
,`thesis_is` varchar(1)
,`is_selected_topic` varchar(1)
,`earn_credit_count` varchar(1)
,`cal_credit_count` varchar(1)
,`student_id_start` varchar(4)
,`student_id_end` varchar(4)
,`semester_start` varchar(1)
,`year_start` varchar(4)
,`semester_end` varchar(1)
,`year_end` varchar(4)
,`open_status` varchar(1)
,`note` varchar(300)
,`input_user` varchar(3)
,`input_date` date
,`pre_check` varchar(200)
);

CREATE TABLE `tbl_cmr30` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `courseno` char(6) NOT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) NOT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) NOT NULL COMMENT 'ตอนปฏิบัติ',
  `max` smallint(6) DEFAULT '0' COMMENT 'จำนวนที่สามารถรับได้',
  `regist_all` smallint(6) DEFAULT '0' COMMENT 'จำนวนนักศึกษาที่ลงทะเบียนได้แล้ว',
  `status` char(1) DEFAULT NULL COMMENT 'สถานะ เปิด=O, ปิด=C',
  `add_onl` smallint(6) DEFAULT '0' COMMENT 'จำนวนนักศึกษาที่รอเพิ่มกระบวนวิชา',
  `drop_onl` smallint(6) DEFAULT '0' COMMENT 'จำนวนนักศึกษาที่รอถอนกระบวนวิชา',
  `move_in_onl` smallint(6) DEFAULT '0' COMMENT 'จำนวนนักศึกษาที่รอย้ายเข้า',
  `move_out_onl` smallint(6) DEFAULT '0' COMMENT 'จำนวนนักศึกษาที่รอย้ายออก',
  `room` varchar(100) DEFAULT NULL COMMENT 'ห้องเรียน',
  `day_id` char(2) DEFAULT NULL COMMENT 'รหัสวันที่ อ้างอิง tbl_day',
  `btime` char(4) DEFAULT NULL COMMENT 'เวลาเริ่มเรียน',
  `ftime` char(4) DEFAULT NULL COMMENT 'เวลาเลิกเรียน',
  `instructor_id` varchar(15) DEFAULT NULL COMMENT 'รหัสอาจารย์ผู้สอนคนที่ 1',
  `bulletin_id` varchar(8) DEFAULT NULL COMMENT 'Bulletin_id ใช้อ้างอิงกับตาราง tbl_bulletin',
  `canchangebulletin` varchar(1) NOT NULL DEFAULT 'Y',
  `study_type` char(1) DEFAULT NULL COMMENT 'รูปแบบการเรียนการสอน ว่าง = เรียนปกติ, 1 = อังกฤษ',
  `study_style` char(1) DEFAULT '0' COMMENT '0:??????, 1: online, 2: ?????online',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `faculty_id` varchar(2) DEFAULT NULL COMMENT 'สำหรับ med เปิดกระบวนวิชา',
  `smile` char(1) DEFAULT NULL,
  `smile_max` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_cmr30_time` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `room` varchar(50) DEFAULT NULL COMMENT 'ห้องเรียน',
  `day_id` char(2) DEFAULT NULL COMMENT 'รหัสวันที่ อ้างอิง tbl_day',
  `btime` char(4) DEFAULT NULL COMMENT 'เวลาเริ่มเรียน',
  `ftime` char(4) DEFAULT NULL COMMENT 'เวลาเลิกเรียน',
  `instructor_id` varchar(15) DEFAULT NULL,
  `input_user` varchar(50) DEFAULT NULL COMMENT 'email',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_condition` (
  `id` int(11) NOT NULL,
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `level_id` char(1) DEFAULT NULL COMMENT 'ระดับการศึกษา',
  `year_id` varchar(50) DEFAULT NULL COMMENT 'รหัสนักศึกษา 2 ตัวแรก(s)',
  `faculty_id` varchar(50) DEFAULT NULL COMMENT 'รหัสคณะ(s)',
  `major_id` varchar(50) DEFAULT NULL COMMENT 'รหัสสาขา(s)',
  `curriculum_id` varchar(50) DEFAULT NULL COMMENT 'รหัสหลักสูตร(s)',
  `major_sub_id` varchar(50) DEFAULT NULL COMMENT 'รหัสสาขาย่อย(s)',
  `project_id` varchar(50) DEFAULT NULL COMMENT 'รหัสโครงการ อ้างอิงตาาง  db_center.tbl_project',
  `except` char(1) DEFAULT '0' COMMENT '0 = ยอมรับ, 1 = ไม่ยอมรับ',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_exam` (
`courseno` varchar(6)
,`examcode` varchar(10)
,`room` varchar(100)
,`term` varchar(3)
,`user` varchar(150)
,`status` varchar(5)
);

CREATE TABLE `tbl_instructor_co` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `courseno` char(6) NOT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) NOT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) NOT NULL COMMENT 'ตอนปฏิบัติ',
  `instructor_id` varchar(15) NOT NULL COMMENT 'รหัสอาจารย์ผู้สอน',
  `input_user` varchar(30) DEFAULT NULL COMMENT 'รหัสผู้บันทึกข้อมูล จาก regist.DEPTUSER',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_abort` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `crelec` decimal(4,2) DEFAULT NULL COMMENT 'เครดิตบรรยาย',
  `crelab` decimal(4,2) DEFAULT NULL COMMENT 'เครดิตปฏิบัติ',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `input_ip` varchar(20) DEFAULT NULL COMMENT 'ip ตอน input',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT NULL COMMENT 'วันที่ที่บันทึก',
  `random_no` int(11) DEFAULT NULL COMMENT 'เลขสุ่ม',
  `reason_id` char(2) DEFAULT NULL COMMENT 'เหตุุผลอ้างอิงจาก tbl_reason',
  `abort_ip` varchar(20) DEFAULT NULL COMMENT 'ip ตอน abort',
  `abort_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `abort_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ยกเลิก'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_del` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `crelec` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตบรรยาย',
  `crelab` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตปฏิบัติ',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip ผู้ใช้',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `delete_user` varchar(50) DEFAULT NULL COMMENT 'ผู้ลบข้อมูล',
  `delete_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ที่ลบข้อมูล',
  `reason_id` char(2) DEFAULT NULL COMMENT 'เหตุผลจากตารารง tbl_reason'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_lab_abort` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `input_ip` varchar(20) DEFAULT NULL COMMENT 'ip ตอน input',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT NULL COMMENT 'วันที่ที่บันทึกข้อมูล',
  `abort_ip` varchar(20) DEFAULT NULL COMMENT 'ip ตอน abort',
  `abort_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `abort_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ยกเลิก'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_lab_del` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip ผู้ใช้',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `delete_user` varchar(50) DEFAULT NULL COMMENT 'ผู้ลบข้อมูล',
  `delete_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ที่ลบข้อมูล',
  `reason_id` char(2) DEFAULT NULL COMMENT 'เหตุผลจากตารารง tbl_reason'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_lab_success` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT NULL COMMENT 'วันที่ที่บันทึกข้อมูล',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip ผู้ใช้',
  `success_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ประมวลผลสำเร็จ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_onl_success` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน A = Add, D = Drop, M = Move',
  `crelec` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตบรรยาย',
  `crelab` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตปฏิบัติ',
  `seclec_move` char(3) DEFAULT NULL COMMENT 'ตอนบรรยายที่ต้องการย้ายไป',
  `seclab_move` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติที่ต้องการย้ายไป',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT NULL COMMENT 'วันที่ที่บันทึก',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip ผู้ใช้',
  `success_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ประมวลผลสำเร็จ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_reason` (
  `reason_id` char(2) NOT NULL COMMENT 'รหัสเหตุผล',
  `reason_name_en` varchar(255) DEFAULT NULL COMMENT 'ชื่อเหตุผล 01 = Clsd, 02 = LabFull, 03 = NoLab, 04 = OvrCr, 05 = UCond, 06 = USeat',
  `reason_name_th` varchar(255) DEFAULT NULL COMMENT 'สาเหตุภาษาไทย'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_reg` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) NOT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) NOT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) NOT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) NOT NULL COMMENT 'ตอนปฏิบัติ',
  `crelec` decimal(4,2) DEFAULT NULL COMMENT 'เครดิตบรรยาย',
  `crelab` decimal(4,2) DEFAULT NULL COMMENT 'เครดิตปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน K = Package, P = Pre, S = คณะ, ภาควิชา',
  `note` varchar(200) DEFAULT NULL COMMENT 'หมายเหตุต่างๆ เช่น ผิดเงื่อนไข',
  `reason_id` char(2) DEFAULT NULL COMMENT 'อ้างอิงตารางเหตุผล tbl_reason',
  `delete_it` char(1) DEFAULT '0' COMMENT '0 = ไม่ลบ,    1 = ลบ',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_reg_del` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `crelec` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตบรรยาย',
  `crelab` decimal(4,2) DEFAULT NULL COMMENT 'หน่วยกิตปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน K = Package, P = Pre, S = คณะ, ภาควิชา',
  `note` varchar(50) DEFAULT NULL COMMENT 'หมายเหตุต่างๆ เช่น ผิดเงื่อนไข',
  `delete_it` char(1) DEFAULT NULL COMMENT '0 = ไม่ลบ,    1 = ลบ',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT NULL COMMENT 'บันทึกข้อมูลเมื่อ',
  `delete_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `delete_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'วันที่ลบช้อมูล',
  `reason_id` char(2) DEFAULT NULL COMMENT 'รหัสสาเหตุการลบจากตาราง tbl_reason'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_reg_lab` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) NOT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) NOT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) NOT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) NOT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน K = Package, P = Pre, S = คณะ, ภาควิชา',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_reg_lab_del` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `courseno` char(6) DEFAULT NULL COMMENT 'รหัสกระบวนวิชา',
  `seclec` char(3) DEFAULT NULL COMMENT 'ตอนบรรยาย',
  `seclab` char(3) DEFAULT NULL COMMENT 'ตอนปฏิบัติ',
  `regist_type` char(1) DEFAULT NULL COMMENT 'ประเภทการลงทะเบียน K = Package, P = Pre, S = คณะ, ภาควิชา',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `delete_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `delete_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่ลบช้อมูล',
  `reason_id` char(2) DEFAULT NULL COMMENT 'รหัสสาเหตุการลบอ้างอิง tbl_reason'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbl_reg_service` (
  `semester` char(1) NOT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) NOT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) NOT NULL COMMENT 'รหัสนักศึกษา',
  `regist_type` char(1) DEFAULT NULL COMMENT 'I = นักศึกษาลงเอง, S = คณะ, ภาควิชา',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ลงทะเบียนใช้บริการของ ป.ตรี';

CREATE TABLE `tbl_reg_service_del` (
  `semester` char(1) DEFAULT NULL COMMENT 'ภาคการศึกษา',
  `year` char(4) DEFAULT NULL COMMENT 'ปีการศึกษา',
  `student_id` char(9) DEFAULT NULL COMMENT 'รหัสนักศึกษา',
  `regist_type` char(1) DEFAULT NULL COMMENT 'I = นักศึกษาลงเอง, S = คณะ, ภาควิชา',
  `input_user` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'บันทึกข้อมูลเมื่อ',
  `delete_uesr` varchar(50) DEFAULT NULL COMMENT 'cmu_account',
  `delete_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'วันที่ที่ลบข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ลงทะเบียนใช้บริการของ ป.ตรี';

ALTER TABLE `tbl_cmr30`
  ADD PRIMARY KEY (`semester`,`year`,`courseno`,`seclec`,`seclab`) USING BTREE,
  ADD KEY `day_id` (`day_id`),
  ADD KEY `faculty_id` (`faculty_id`),
  ADD KEY `instructor_id` (`instructor_id`),
  ADD KEY `bulletin_id` (`bulletin_id`),
  ADD KEY `courseno` (`courseno`),
  ADD KEY `semester` (`semester`),
  ADD KEY `year` (`year`),
  ADD KEY `seclec` (`seclec`),
  ADD KEY `seclab` (`seclab`);

ALTER TABLE `tbl_cmr30_time`
  ADD KEY `semester` (`semester`,`year`,`courseno`,`seclec`,`seclab`,`day_id`,`instructor_id`);

ALTER TABLE `tbl_condition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semester` (`semester`,`year`,`courseno`,`seclec`,`seclab`,`level_id`,`year_id`,`faculty_id`,`major_id`,`curriculum_id`,`major_sub_id`,`project_id`);

ALTER TABLE `tbl_instructor_co`
  ADD PRIMARY KEY (`semester`,`year`,`courseno`,`seclec`,`seclab`,`instructor_id`);

ALTER TABLE `tbl_onl_abort`
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`) USING BTREE;

ALTER TABLE `tbl_onl_del`
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`) USING BTREE;

ALTER TABLE `tbl_onl_lab_abort`
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`);

ALTER TABLE `tbl_onl_lab_del`
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`) USING BTREE;

ALTER TABLE `tbl_onl_lab_success`
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`);

ALTER TABLE `tbl_onl_success`
  ADD KEY `index_view_log` (`semester`,`year`,`student_id`,`input_date`);

ALTER TABLE `tbl_reason`
  ADD PRIMARY KEY (`reason_id`);

ALTER TABLE `tbl_reg`
  ADD PRIMARY KEY (`semester`,`year`,`student_id`,`courseno`,`seclec`,`seclab`),
  ADD KEY `tbl_reg_semester_year_courseno_index` (`semester`,`year`,`courseno`),
  ADD KEY `tbl_reg_student_id_index` (`student_id`);

ALTER TABLE `tbl_reg_lab`
  ADD PRIMARY KEY (`semester`,`year`,`student_id`,`courseno`,`seclec`,`seclab`),
  ADD KEY `tbl_reg_lab_semester_year_courseno_index` (`semester`,`year`,`courseno`);

ALTER TABLE `tbl_reg_service`
  ADD PRIMARY KEY (`semester`,`year`,`student_id`);


ALTER TABLE `tbl_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11435;
