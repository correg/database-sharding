data "kustomization_build" "vitess" {
  path = "https://github.com/planetscale/vitess-operator/deploy"
}

resource "k8s_manifest" "vitess-deployment" {
  for_each = data.kustomization_build.vitess.ids

  content = data.kustomization_build.vitess.manifests[each.value]
  namespace = "default"
}
