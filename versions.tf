terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
    k8s = {
      source = "banzaicloud/k8s"
    }
    kustomization = {
      source = "kbst/kustomization"
    }
  }

  required_version = ">= 0.13"
}