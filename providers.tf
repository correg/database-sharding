variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

provider "k8s" {
  load_config_file = "false"

  host = digitalocean_kubernetes_cluster.correg-vitess-cluster.endpoint

  token = digitalocean_kubernetes_cluster.correg-vitess-cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.correg-vitess-cluster.kube_config[0].cluster_ca_certificate
  )
}

provider "kustomization" {
  kubeconfig_raw = digitalocean_kubernetes_cluster.correg-vitess-cluster.kube_config[0].raw_config
}
