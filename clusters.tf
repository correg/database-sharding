resource "digitalocean_kubernetes_cluster" "correg-vitess-cluster" {
  name   = "correg-vitess-cluster"
  region = "sgp1"
  # Grab the latest version slug from `doctl kubernetes options versions`
  version = "1.19.3-do.2"

  node_pool {
    name       = "main-pool"
    size       = "s-2vcpu-4gb"
    node_count = 3
  }
}