#!/bin/bash

for id in $(doctl compute volume list --format ID --no-header)
do
  doctl compute volume delete $id --force
done